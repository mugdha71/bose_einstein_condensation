from matplotlib import pylab


def readFile(file):
    value = ""
    data_array = []
    data = []
    with open(file, 'r') as f:
        c = f.read(1)
        i = 0
        while True:
            if c == ',' or c == ']':
                try:
                    data.append(float(value))
                except:
                    pass
                value = ""
            elif c == '\n':
                data_array.append(data)
                data = []
                i += 1
                if i == 6:
                    break
            elif c == '[':
                pass
            else:
                value += c
            try:
                c = f.read(1)
            except:
                pass
    list_x = data_array[0]
    y = data_array[1]
    data_x = data_array[2]
    data_x_l = data_array[3]
    print(len(data_x_l))
    data_y = data_array[4]
    data_y_l = data_array[5]
    return list_x, y, data_x, data_x_l, data_y, data_y_l


def readData(file):
    with open(file, 'r') as f:
        lines = f.readline()
        list_x = list(map(float, lines[1:-2].split(',')))
        lines = f.readline()
        y = list(map(float, lines[1:-2].split(',')))
        # lines = f.readline()
        # data_x = list(map(float, lines[1:-2].split(',')))
        # lines = f.readline()
        # data_x_l = list(map(float, lines[1:-2].split(',')))
        # lines = f.readline()
        # data_y = list(map(float, lines[1:-2].split(',')))
        # lines = f.readline()
        # data_y_l = list(map(float, lines[1:-2].split(',')))

    data_x = []
    data_x_l = []
    data_y = []
    data_y_l = []
    return list_x, y, data_x, data_x_l, data_y, data_y_l


def plot(N, T, list_x, y, data_x, data_x_l):
    pylab.plot(list_x, y, '--k', linewidth=2.0, label='Ground state')
    try:
        pylab.hist(data_x, color='#0368ff', density=True, bins=120, alpha=0.7, label='All bosons')
    except:
        pass
    pylab.hist(data_x_l, color='#6eff03', density=True, bins=120, alpha=0.6, label='Bosons in longer cycle')
    pylab.xlim(-3.0, 3.0)
    pylab.xlabel('$x$', fontsize=14)
    pylab.ylabel('$\pi(x)$', fontsize=14)
    pylab.title('3-d non-interacting bosons $x$ distribution $N= %i$, $T= %.3f$' % (N, T))
    pylab.legend()
    pylab.savefig('pic_bw/new_position_distribution_N%i_T%.3f.svg' % (N, T))
    # pylab.show()
    pylab.close()


if __name__ == '__main__':
    t = [1,5]
    for i in t:
        file_name = "%.1f.txt" % (i * .1)
        print(file_name)
        # readFile(file_name)
        list_x, y, data_x, data_x_l, data_y, data_y_l = readFile(file_name)
        plot(512, i * 0.1, list_x, y, data_x, data_x_l)
