import matplotlib.pyplot as plt
t = []
Ns = []
n = []
with open('n0_n_vs_T.txt', 'r') as f:
    lines = f.readlines()
    head = lines[0].split(",")[1:-1]
    print(head)
    Ns = [float(i) for i in head]
    for line in lines[1:]:
        line_sp = line.split(',')[:-1]
        t.append(float(line_sp[0]))
        n.append( [float(i) for i in line_sp[1:]])

# print(l)
print(Ns)
nn = list(map(list, zip(*n)))
# print(nn)
ts = []
for i in Ns:
    tt = []
    for j in t:
        tt.append(j/i**(1/3))
    ts.append(tt)


for i in range(len(Ns)):
    plt.plot(ts[i], nn[i])
plt.show()

if __name__ == '__main__':
    pass