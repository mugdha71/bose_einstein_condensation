import math, numpy, pylab
import matplotlib.pyplot as plt

# Free off-diagonal density matrix
def rho_free(x, xp, beta):
    return (math.exp(-(x - xp) ** 2 / (2.0 * beta)) /
            math.sqrt(2.0 * math.pi * beta))

# Harmonic density matrix in the Trotter approximation (returns the full matrix)
def rho_harmonic_trotter(grid, beta):
    return numpy.array([[rho_free(x, xp, beta) * \
                         numpy.exp(-0.5 * beta * 0.5 * (x ** 2 + xp ** 2)) \
                         for x in grid] for xp in grid])
def start_k(T_star = 0.5):
    N = 700#number of bosons
    # T_star = 0.5 #temperature
    beta = 1.0 / N ** (1.0 / 3.0) / T_star
    Z = canonic_recursion(N, beta) #partition function
    # pi_k = [(z(k, beta) * Z[N - k] / Z[-1]) / float(N) for k in range(1, N + 1)]  # probability of a cycle of length k
    pi_k = [((z(k, beta) * Z[N - k] / Z[-1]) / float(N))*1000 for k in range(1, N + 1)]  # probability of a cycle of length k

    # graphics output
    pylab.plot(range(1, N + 1), pi_k, lw=1.5)
    pylab.ylim(0.0, 2.5)
    pylab.xlabel('cycle length $k$', fontsize=16)
    pylab.ylabel('cycle probability $\pi_k$ ($x10^{-3}$)', fontsize=16)
    pylab.title('Cycle length distribution ($N=%i$)' % (N), fontsize=16)
    pylab.pause(1)
    # pylab.savefig('plot-prob_cycle_length.png')

def start(T):
    pylab.clf()
    x_max = 5.0
    nx = 500
    dx = 2.0 * x_max / (nx - 1)
    x = [i * dx for i in range(int(-(nx - 1) / 2), int(nx / 2 + 1))]
    beta_tmp = 2.0 ** (-8)  # initial value of beta (power of 2)
    # beta = 2.0 ** -1                      # actual value of beta (power of 2)
    beta = 1.0 / 700 ** (1.0 / 3.0) / T
    rho = rho_harmonic_trotter(x, beta_tmp)  # density matrix at initial beta
    while beta_tmp < beta:
        rho = numpy.dot(rho, rho)
        rho *= dx
        beta_tmp *= 2.0

    # graphics output
    pylab.imshow(rho, extent=[-x_max, x_max, -x_max, x_max], origin='lower',cmap=plt.cm.Blues)
    pylab.colorbar()
    pylab.title('$T = %0.2f K$' %T)
    pylab.xlabel('$x$', fontsize=18)
    pylab.ylabel('$x\'$', fontsize=18)
    pylab.savefig('matrix/plot-harmonic-rho_%0.2f.svg' %T)
    pylab.pause(0.1)

if __name__ == '__main__':
    list =[0.001,0.002,0.1,0.2,0.3,0.4,0.5,0.8,1,5,10,15,20,30]
    list.reverse()
    for i in list:
        start(i)
    pylab.show()
    pass