import math, random, pylab, numpy as np


def z(beta):
    return 1.0 / (1.0 - math.exp(- beta))


def pi_two_bosons(x, beta):  # exact two boson position distribution
    pi_x_1 = math.sqrt(math.tanh(beta / 2.0)) / math.sqrt(math.pi) * math.exp(-x ** 2 * math.tanh(beta / 2.0))
    pi_x_2 = math.sqrt(math.tanh(beta)) / math.sqrt(math.pi) * math.exp(-x ** 2 * math.tanh(beta))
    weight_1 = z(beta) ** 2 / (z(beta) ** 2 + z(2.0 * beta))
    weight_2 = z(2.0 * beta) / (z(beta) ** 2 + z(2.0 * beta))
    pi_x = pi_x_1 * weight_1 + pi_x_2 * weight_2
    return pi_x


def levy_harmonic_path(k):
    x = [random.gauss(0.0, 1.0 / math.sqrt(2.0 * math.tanh(k * beta / 2.0)))]
    if k == 2:
        Ups1 = 2.0 / math.tanh(beta)
        Ups2 = 2.0 * x[0] / math.sinh(beta)
        x.append(random.gauss(Ups2 / Ups1, 1.0 / math.sqrt(Ups1)))
    return x[:]


def rho_harm_1d(x, xp, beta):
    Upsilon_1 = (x + xp) ** 2 / 4.0 * math.tanh(beta / 2.0)
    Upsilon_2 = (x - xp) ** 2 / 4.0 / math.tanh(beta / 2.0)
    return math.exp(- Upsilon_1 - Upsilon_2)


# beta = 2.0
N = 256
T_star = 3
beta = 1.0 / (T_star * N ** (1.0 / 3.0))
list_beta = np.linspace(0.1, 5.0)
nsteps = 100000
low = levy_harmonic_path(2)
high = low[:]
fract_one_cycle_dat, fract_two_cycles_dat = [], []

for beta in list_beta:
    one_cycle_dat = 0.0  # initialise the permutation fractions for each temperature
    data = []
    for step in range(nsteps):
        # move 1 (direct-sample the positions)
        if low[0] == high[0]:  # if the cycle is of length 1
            k = random.choice([0, 1])
            low[k] = levy_harmonic_path(1)[0]
            high[k] = low[k]  # assures the cycle
        else:  # if the cycle is of length 2s
            low[0], low[1] = levy_harmonic_path(2)
            high[1] = low[0]  # assures the cycle
            high[0] = low[1]
            one_cycle_dat += 1.0 / float(nsteps)  # calculate the fraction of the single cycle cases
        data += low[:]  # save the position histogram data
        # move 2 (Metropolis for sampling the permutations)
        weight_old = (rho_harm_1d(low[0], high[0], beta) * rho_harm_1d(low[1], high[1], beta))
        weight_new = (rho_harm_1d(low[0], high[1], beta) * rho_harm_1d(low[1], high[0], beta))
        if random.uniform(0.0, 1.0) < weight_new / weight_old:
            high[0], high[1] = high[1], high[0]

    fract_one_cycle_dat.append(one_cycle_dat)
    fract_two_cycles_dat.append(1.0 - one_cycle_dat)  # save the fraction of the two cycles cases

# Exact permutation distributions for all temperatures
fract_two_cycles = [z(beta) ** 2 / (z(beta) ** 2 + z(2.0 * beta)) for beta in list_beta]
fract_one_cycle = [z(2.0 * beta) / (z(beta) ** 2 + z(2.0 * beta)) for beta in list_beta]

# Graphics output:
list_x = [0.1 * a for a in range(-30, 31)]
y = [pi_two_bosons(a, beta) for a in list_x]
pylab.plot(list_x, y, linewidth=2.0, label='Exact distribution')
pylab.hist(data, density=True, bins=80, label='QMC', alpha=0.5, color='green')
pylab.legend()
pylab.xlabel('$x$', fontsize=14)
pylab.ylabel('$\\pi(x)$', fontsize=14)
pylab.title('2 non-interacting bosonic 1-d particles', fontsize=14)
pylab.xlim(-3, 3)
# pylab.savefig('plot_A2_beta%s.png' % beta)
pylab.show()
pylab.clf()

fig = pylab.figure(figsize=(10, 5))

ax = fig.add_subplot(1, 2, 1)
ax.plot(list_beta, fract_one_cycle_dat, linewidth=4, label='QMC')
ax.plot(list_beta, fract_one_cycle, linewidth=2, label='exact')
ax.legend()
ax.set_xlabel('$\\beta$', fontsize=14)
ax.set_ylabel('$\\pi_2(\\beta)$', fontsize=14)
ax.set_title('Fraction of cycles of length 2', fontsize=14)

ax = fig.add_subplot(1, 2, 2)
ax.plot(list_beta, fract_two_cycles_dat, linewidth=4, label='QMC')
ax.plot(list_beta, fract_two_cycles, linewidth=2, label='exact')
ax.legend()
ax.set_xlabel('$\\beta$', fontsize=14)
ax.set_ylabel('$\\pi_1(\\beta)$', fontsize=14)
ax.set_title('Fraction of cycles of length 1', fontsize=14)

# pylab.savefig('plot_A2.png')
pylab.show()
pylab.clf()
