from random import *
from matplotlib import pyplot as plt

lis = {}
for i in range(1, 101):
    lis[i] = 0
for i in range(10000):
    x = uniform(1, 10)
    lis[int(x)] += 1
x = []
y = []
for i in range(1, 10):
    x.append(i)
    y.append(lis[i])


plt.scatter(x, y)
plt.show()
