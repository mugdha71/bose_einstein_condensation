import random

import math
from matplotlib import pyplot

y = []
x = []
all_x = []
all_y = []
for j in range(10):
    for i in range(100):
        y.append(i+random.uniform(-10,+10))
        x.append(i)
    all_x.append(x)
    all_y.append(y)
    x = []
    y = []

all_sd = []
for i in range(100):
    p = 0
    for j in range(10):
        p += all_y[j][i]
    p /= 10
    sd = 0
    for j in range(10):
        sd += (all_y[j][i] - p)**2
    sd /= 10
    sd = math.sqrt(sd)
    all_sd.append(sd)
    y.append(p)

all_x2 = []
all_y2 = []
for j in range(10):
    x = []
    y = []
    for i in range(100):
        y.append(i+random.uniform(-10,+10))
        x.append(i)
    all_x2.append(x)
    all_y2.append(y)


all_sd2 = []
y2 = []
for i in range(100):
    p = 0
    for j in range(10):
        p += all_y2[j][i]
    p /= 10
    sd = 0
    for j in range(10):
        sd += (all_y2[j][i] - p)**2
    sd /= 10
    sd = math.sqrt(sd)
    all_sd2.append(sd)
    y2.append(p)


print(all_sd)
print(all_sd2)
z = []
for i, j in zip(y,y2):
    z.append(i-j)

pyplot.errorbar(all_x[0],y,all_sd,linestyle='None', marker='^')
pyplot.errorbar(all_x2[0],y2,all_sd2,linestyle='None', marker='^')
pyplot.errorbar(all_x2[0],z)
pyplot.show()

if __name__ == "__main__":
    pass

