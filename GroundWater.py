#!/usr/bin/python3.7
import math
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# global variables
nx = 100
ny = 50
ni = 5


def relax2d(p, hx, hy, u, d, s):
    h2 = hx * hx
    a = h2 / (hy * hy)
    b = 1 / (4 * (1 + a))
    ab = a * b
    q = 1 - p
    for i in range(1, nx):
        for j in range(1, ny):
            xp = b * (d[i + 1][j] / d[i][j] + 1)
            xm = b * (d[i - 1][j] / d[i][j] + 1)
            yp = ab * (d[i][j + 1] / d[i][j] + 1)
            ym = ab * (d[i][j - 1] / d[i][j] + 1)
            u[i][j] = q * u[i][j] + p * (xp * u[i + 1][j]
                                         + xm * u[i - 1][j] + yp * u[i][j + 1]
                                         + ym * u[i][j - 1] + h2 * s[i][j])


def main():
    sigma0 = 1
    a = -0.04
    phi0 = 200
    b = -20
    lx = 1000
    hx = lx / nx
    ly = 500
    hy = ly / ny
    phi = []
    sigma = []
    f = []
    p = 0.5

    # temp array to create 2 dimensional array
    sigma_j = []
    phi_j = []
    f_j = []

    # Set up boundary values and a trial solution
    for i in range(nx+1):
        x = i * hx
        for j in range(ny+1):
            y = j*hy
            sigma_j.append(sigma0 + a * y)
            # calculate phi and store
            phi_j.append(phi0 + b * math.cos(math.pi * x / lx) * y / ly)
            f_j.append(0)
        sigma.append(sigma_j)
        f.append(f_j)
        phi.append(phi_j)
        phi_j = []
        f_j = []
        sigma_j = []

    for step in range(ni):
        # Ensure boundary conditions by 4-point formula
        for j in range(ny):
            phi[0][j] = (4 * phi[1][j] - phi[2][j]) / 3
            phi[nx][j] = (4 * phi[nx - 1][j] - phi[nx - 2][j]) / 3
        relax2d(p, hx, hy, phi, sigma, f)

    # variables for 3d plot
    x_data = []
    y_data = []
    z_data = []

    # Output the result
    for i in range(nx+1):
        x = i * hx
        for j in range(ny+1):
            y = j*hy
            x_data.append(x)
            y_data.append(y)
            z_data.append(phi[i][j])

    fig = plt.figure()
    axes_3d = fig.gca(projection='3d')
    axes_3d.plot(x_data, y_data, z_data, linewidth=0.2, antialiased=True)
    axes_3d.set_xlabel("x")
    axes_3d.set_ylabel("y")
    axes_3d.set_zlabel("phi")

    plt.show()

if __name__ == "__main__":
    main()
