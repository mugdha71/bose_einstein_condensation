import matplotlib.pyplot as plt
from matplotlib import pylab
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

lim = 5
X = np.arange(-1*lim, lim, lim/100.0)
Y = np.arange(-1*lim, lim, lim/100.0)
X, Y = np.meshgrid(X, Y)
R = X**2 + Y**2
Z = R*0.5

fig = plt.figure()
ax = Axes3D(fig)
ax.plot_surface(X,Y,Z, cmap=pylab.cm.viridis,linewidth=0, antialiased=False)
ax.set_xlabel(r"$x$")
ax.set_ylabel(r"$y$")
ax.set_zlabel(r"$V = \frac{1}{2} (x^2+y^2) $")
plt.savefig("3d_potential_well.svg")

plt.show()

if __name__ == '__main__':
    pass
