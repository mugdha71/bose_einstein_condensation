"""
Assignment #2
Simulate the Magnetisation properties of material with Ising Model via Metropolis Method
Name : Khan MAhmud Enamul Hasan
Reg: 2014132010
"""
#Initialization (Module importing & Function defining) portion of the code.'''

import numpy as np
from numpy.random import rand
import matplotlib.pyplot as plt

N = 0
interaction = 1 # Interaction (ferromagnetic if positive, antiferromagnetic if negative)
magnetic_moment = 1
ex_mag_field = 0

last_energy = 0


def initial_state (n):
    """
    Generates a random spin configuration for initial condition
    :param n: Size of the lattice, N x N
    :return: spin state
    """
    spin_state = 2 * np.random.randint(2, size=(n, n)) - 1
    return spin_state


def metropolis (ensemble, beta, N):
    """
    Metropolis algorithm
    :param ensemble:
    :param beta:
    :param N: Size of the lattice, n x n
    :return: ensemble
    """
    for i in range(N):
        for j in range(N):
            a = np.random.randint(0, N)
            b = np.random.randint(0, N)
            s =  ensemble [a, b]
            nb = ensemble [(a+1)%N,b] + ensemble[a,(b+1)%N] + ensemble[(a-1)%N,b] + ensemble[a,(b-1)%N]
            cost = 2 * s * nb
            if cost < 0:
              s *= -1
            elif rand() < np.exp (-cost * beta):
              s *= -1
            ensemble [a, b] = s
    return ensemble


def calc_energy (ensemble):
    """
    Calculates Energy of a given ensemble.
    :param ensemble:
    :return: energy
    """
    energy = 0
    for i in range(len(ensemble)):
        for j in range(len(ensemble)):
            s = ensemble [i,j]
            nb = ensemble [(i+1)%N, j] + ensemble [i,(j+1)%N] + ensemble [(i-1)%N, j] + ensemble [i,(j-1)%N]
            energy += -nb*s
    return energy/4


def Calc_Mag (ensemble):
    """
    Calculates Magnetization of a given ensemble.
    :param ensemble:
    :return:
    """
    mag = np.sum(ensemble)
    return mag


def ising_spin_field (n):
    """
    Generates a random spin field of n x n lattice.
    :param n: Size of the lattice
    :return:
    """
    return np.random.choice([-1, 1], size=(n, n))


def get_probability (e1, e2, temperature):
    """
    Generates the probability of the system being in an equilibrium state
    :param e1: Current Energy
    :param e2: New Energy
    :param temperature: Temperature (in units of energy)
    :return:
    """
    return np.exp((e1 - e2) / temperature)


def get_energy(spins):
    """
    Generates energy states for the spin field
    :param spins: Spins
    :return: Total Energy
    """
    return -np.sum(
        interaction * spins * np.roll(spins, 1, axis=0) + interaction * spins * np.roll(spins, -1, axis=0) +
        interaction * spins * np.roll(spins, 1, axis=1) + interaction * spins * np.roll(spins, -1, axis=1)) / 2 - magnetic_moment * np.sum(ex_mag_field * spins)


def ising_update (s, temp):
    """
    Gets updated spin pattern from the previous iteration
    :param s: Spins
    :param temp: Temperature (in units of energy)
    :return: Spins
    """
    new_spins = np.copy(s)
    i = np.random.randint(s.shape[0])
    j = np.random.randint(s.shape[1])
    new_spins [i, j] *= -1

    global last_energy
    current_energy = get_energy (s)
    new_energy = get_energy (new_spins)
    if get_probability (current_energy, new_energy, temp) > np.random.random():
        last_energy = new_energy
        return new_spins
    else:
        last_energy = current_energy
        return s


def start(n = 6,size = 50, t_steps = 10, eqb_steps =  100, mc_steps = 10**10):
    """
    :param n: Size of the lattice, n x n
    :param size: Total size - n
    :param t_steps: Number of Temperature points to be plotted
    :param eqb_steps: Number of Monte Carlo sweeps for equilibration
    :param mc_steps: Number of Monte Carlo sweeps for calculation
    :return:
    """
    global ex_mag_field
    global N
    N = n
    T = np.linspace(1, 6, t_steps) # Temperature range
    E = np.zeros(t_steps) # Energy,
    M = np.zeros(t_steps) # Magnetization
    C = np.zeros(t_steps) # Specific Heat,
    X = np.zeros(t_steps) # Magnetic Susceptibility
    lattice_shape = (n + size, n + size) # Lattice size
    spins = ising_spin_field (n + size) # Spin configuration
    ex_mag_field = np.full(lattice_shape, 0) # External magnetic field
    temperature = .5 # Temperature (in units of energy)
    n1, n2 = 1.0 / (mc_steps * n * n), 1.0 / (mc_steps ** 2 * n ** 2)

    # Dividing by number of Samples, and by Ensemble size to get intensive values
    for z in range (t_steps):
        E1 = M1 = E2 = M2 = 0
        ensemble = initial_state(n)
        iT = 1.0 / T[z]
        iT_2 = iT ** 2

        for i in range (eqb_steps):              # Equilibrate the system
            metropolis (ensemble, iT, n)            # Through Monte Carlo moves

        for i in range (eqb_steps):
            metropolis (ensemble, iT, n)
            Egy = calc_energy (ensemble)         # Calculating the Energy
            Mag = Calc_Mag (ensemble)            # Calculating the Magnetisation

            E1 = E1 + Egy
            M1 = M1 + Mag
            M2 += Mag ** 2
            E2 += Egy ** 2

        E[z] = n1 * E1
        M[z] = n1 * M1
        C[z] = (n1 * E2 - n2 * E1 ** 2) * iT_2
        X[z] = (n1 * M2 - n2 * M1 ** 2) * iT


    """
    Plotting figures.
    """


    fig = plt.figure(figsize=(50, 50)) # plot the calculated values

    sub_plt =  fig.add_subplot(2, 2, 1)
    plt.scatter(T, abs(M), s=20, marker='x', color='crimson', label = 'Magnetization v/s Temperature')
    plt.xlabel("Temperature (T)", fontsize=10)
    plt.ylabel("Magnetization (M) ", fontsize=10)
    plt.legend(fancybox=True, framealpha=0.8)
    #plt.title('Magnetization v/s Temperature', fontsize = 8, fontweight = 'bold', color = 'darkslategray')
    plt.grid()
    plt.axis('tight')
    #plt.show()

    sub_plt =  fig.add_subplot(2, 2, 2)
    plt.scatter(T, E, s=20, marker='x', color='red', label = 'Energy v/s Temperature')
    plt.xlabel("Temperature (T)", fontsize=10)
    plt.ylabel("Energy (E)", fontsize=10)
    plt.legend(fancybox=True, framealpha=0.8)
    #plt.title('Energy v/s Temperature', fontsize = 8,  fontweight = 'bold', color = 'darkslategray')
    plt.grid()
    plt.axis('tight')
    #plt.show()

    sub_plt =  fig.add_subplot(2, 2, 3)
    plt.scatter(T, C, s=20, marker='x', color='darkslategray', label = 'Specific Heat v/s Temperature')
    plt.xlabel("Temperature (T)", fontsize=10)
    plt.ylabel("Specific Heat (Cv)", fontsize=10)
    plt.legend(fancybox=True, framealpha=0.8)
    plt.grid()
    plt.axis('tight')

    sub_plt =  fig.add_subplot(2, 2, 4)
    plt.scatter(T, X, s=20, marker='x', color='darkblue', label = 'Magnetic Susceptibility v/s Temperature')
    plt.xlabel("Temperature (T)", fontsize=10)
    plt.ylabel("Magnetic Susceptibility (χ)", fontsize=10)
    plt.legend(fancybox=True, framealpha=0.8)
    plt.axis('tight')
    plt.grid()
    plt.show()

    # Animates the spin field alignment variation
    im = plt.imshow(spins, cmap = 'gray')
    t = 0
    cb = plt.colorbar(im)

    fg_color = 'black'
    im.axes.tick_params(color=fg_color, labelcolor=fg_color)
    # Set imshow outline
    for spine in im.axes.spines.values():
        spine.set_edgecolor(fg_color)
    cb.set_label('colorbar label', color=fg_color)



    # Set tick and ticklabel color
    im.axes.tick_params(color=fg_color, labelcolor=fg_color)


    # Set colorbar label plus label color
    cb.set_label('colorbar label', color=fg_color)

    # Set colorbar tick color
    cb.ax.yaxis.set_tick_params(color=fg_color)

    # Set colorbar edgecolor
    cb.outline.set_edgecolor(fg_color)

    # Set colorbar ticklabels
    plt.setp(plt.getp(cb.ax.axes, 'yticklabels'), color=fg_color)

    fig.patch.set_facecolor(fg_color)
    plt.tight_layout()

    while t<mc_steps:
        if t % 1000 == 0:
            im.set_data(spins)
            plt.pause(10e-100) # For faster transition into equibrium.
            print(last_energy)
        t+=1
        spins = ising_update(spins, temperature)

    plt.show()
start()