import math, numpy as np, pylab as plt



# calculate the partition function for 5 bosons by stacking the bosons in one of the N_states
# number of possible states and counting only a specific order of them (they are indistinguishable)
def bosons_bounded_harmonic(beta, N):
    Energy = []  # initialise the vector that the energy values are saved with enumeration
    n_states_1p = 0  # initialise the total number of single trapped boson states
    for n in range(N + 1):
        degeneracy = (n + 1) * (n + 2) / 2.0  # degeneracy in the 3D harmonic oscillator
        Energy += [float(n)]*int(degeneracy)
        n_states_1p += int(degeneracy)

    n_states_5p = 0  # initialise the total number states of 5 trapped bosons
    Z = 0.0  # initialise the partition function
    N0_mean = 0.0
    E_mean = 0.0
    for s_0 in range(n_states_1p):
        for s_1 in range(s_0, n_states_1p):  # consider the order s_0<s_1... to avoid overcounting
            for s_2 in range(s_1, n_states_1p):
                for s_3 in range(s_2, n_states_1p):
                    for s_4 in range(s_3, n_states_1p):
                        n_states_5p += 1
                        state = [s_0, s_1, s_2, s_3, s_4]  # construct the state of each 5 boson
                        E = sum(Energy[s] for s in state)  # calculate the total energy by above enumeration
                        Z += math.exp(-beta * E)  # canonical partition function
                        E_mean += E * math.exp(-beta * E)  # avg. total energy
                        N0_mean += state.count(0) * math.exp(-beta * E)  # avg. ground level occupation number
    return n_states_5p, Z, E_mean, N0_mean


N = 4  # the energy cutoff for each boson
beta = 1.0  # inverse temperature

n_states_5p, Z, E_mean, N0_mean = bosons_bounded_harmonic(beta, N)

print(
'Temperature:', 1 / beta, 'Total number of possible states:', n_states_5p, '| Partition function:', Z, \
'| Average energy per particle:', E_mean / Z / 5.0, \
'| Condensate fraction (ground state occupation per particle):', N0_mean / Z / 5.0
)
cond_frac = []
temperature = []
for T in np.linspace(0.1, 1.0, 10):
    n_states_5p, Z, E_mean, N0_mean = bosons_bounded_harmonic(1.0 / T, N)
    cond_frac.append(N0_mean / Z / 5.0)
    temperature.append(T)

plt.plot(temperature, cond_frac)
plt.title('Condensate? fraction for the $N=5$ bosons bounded trap model ($N_{bound}=%i$)' % N, fontsize=14)
plt.xlabel('$T$', fontsize=14)
plt.ylabel('$\\langle N_0 \\rangle$ / N', fontsize=14)
plt.grid()
plt.show()
if __name__ == '__main__':
    pass