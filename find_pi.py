import datetime
import random

def dart():
    x = random.uniform(-1,1)
    y = random.uniform(-1,1)
    r = 1
    if x*x + y*y <= 1:
        return True
    return False


def main():
    x = 0
    t = 10**10
    i = 10**10
    ts = datetime.datetime.now()
    for i in range(t):
        if(dart()):
            x += 1
    tf = datetime.datetime.now()
    te = tf - ts
    print(4*x/t)
    print(te)


if __name__ == "__main__":
    main()
