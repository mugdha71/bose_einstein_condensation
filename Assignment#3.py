"""
Initialization (Module importing & Function defining) portion of the code.

"""
import numpy as np
import matplotlib.pyplot as plt


def psi_trial (x, alpha):
    """
    Computes the trial wavefunction.
    :param x:
    :param alpha:
    :return:
    """
    return np.exp(-alpha * x ** 2) # Assuming it is Gaussian


def get_prob_density(x, alpha):
    """
    Generates probability density of the trial wavefunction.
    :param x:
    :param alpha:
    :return:
    """
    return psi_trial (x, alpha) ** 2 / np.sqrt(np.pi / alpha)


def get_local_energy (x, alpha):
    """
    Generates the local energy, in terms of x and alpha, corresponding to the trial wavefunction.
    :param x:
    :param alpha:
    :return:
    """
    return alpha + x ** 2 * (1/2 - 2 * alpha ** 2)


def variational_monte_carlo (N, alpha):
    """
    Quantum Variational Monte Carlo algorithm
    :param N:
    :param alpha:
    :return:
    """
    eln_average = 0                          # <Eln> = <E_L * (dln(psi_trial)/dalpha)> (Average)
    ln_average = 0                           # <ln>  = <(dln(psi_trial)/dalpha)> (Average)
    e = 0                                    # Upper bound limit of ground state
    e2 = 0                                   # Squared Upper bound limit of ground state
    error_tolerance = 0                      # Maximum admissiable errors or MC step rejections
    sigma = 3 / np.sqrt(2 * alpha)           # Length chosen for Markov chain within the VMC method
    x = np.random.rand() * 2 * sigma - sigma # Random time-step generator
    for i in range(N):
        x_trial = x + 0.4 * (np.random.rand() * 2 * sigma - sigma)
        # x_trial = np.random.rand()*2*L-L
        if get_prob_density (x_trial, alpha) >= get_prob_density (x, alpha):
            x = x_trial
        else:
            dummy = np.random.rand()
            if dummy < get_prob_density (x_trial, alpha) / get_prob_density (x, alpha):
                x = x_trial
            else:
                error_tolerance += 1 / N
        e += get_local_energy (x, alpha) / N
        e2 += get_local_energy (x, alpha) ** 2 / N
        eln_average += (get_local_energy (x, alpha) * -x ** 2) / N
        ln_average += -x ** 2 / N

    return e, e2, eln_average, ln_average, error_tolerance

def plot_energy(energy_plot, energy_analytical_plot):
    plt.plot(energy_plot, color='steelblue', linewidth=3, label='E exp Value v/s Timestep')
    plt.xlabel("Timestep", fontsize=10)
    plt.ylabel("E exp value", fontsize=10)
    plt.legend(fancybox=True, framealpha=0.8)
    plt.grid()
    plt.axis('tight')

    # sbplt =  fig1.add_subplot(2, 1, 2)
    plt.plot(energy_analytical_plot, color='lightcoral', linewidth=3, label='E exp Value analytical v/s Timestep')
    plt.xlabel("Timestep", fontsize=10)
    plt.ylabel("E exp value analytical", fontsize=10)
    plt.legend(fancybox=True, framealpha=0.8)
    plt.grid()
    plt.axis('tight')
    plt.show()


def plot_variance(variance_plot,var_analytical_plot):
    plt.plot(variance_plot, color='orangered', linewidth=3, label='Var E v/s Timestep')
    plt.xlabel("Timestep", fontsize=10)
    plt.ylabel("Var E", fontsize=10)
    plt.legend(fancybox=True, framealpha=0.8)
    plt.grid()
    plt.axis('tight')

    # sbplt =  fig2.add_subplot(2, 1, 2)
    plt.plot(var_analytical_plot, color='darkmagenta', linewidth=3, label='Var E analytical v/s Timestep')
    plt.xlabel("Timestep", fontsize=10)
    plt.ylabel("Var E analytical", fontsize=10)
    plt.legend(fancybox=True, framealpha=0.8)
    plt.grid()
    plt.axis('tight')
    plt.show()

def plot_alpha(alpha_plot, energy_plot, energy_analytical_plot):
    fig3 = plt.figure(figsize=(12, 8))  # plot the calculated values
    # sbplt = fig3.add_subplot(1,1,1)
    plt.title('Harmonic Oscillator: Energy vs. α')
    plt.grid()
    plt.plot(alpha_plot, energy_plot, linewidth=3, color='royalblue', label='Numerical Approach')
    plt.plot(alpha_plot, energy_analytical_plot, linewidth=3, color='firebrick', label='Analytical Approach')
    plt.legend(fancybox=True, framealpha=0.8)
    plt.xlabel('α')
    plt.ylabel('Energy')
    plt.axis('tight')
    plt.show()


def get_string(array):
    array_string = ""
    for i in array:
        array_string += str(i)+","
    array_string += "\n"
    return array_string

def save_in_file(energy_plot, energy_analytical_plot, variance_plot, var_analytical_plot, alpha_plot, name ="data"):
    file = open(name+".txt", "w")
    file.write(get_string(energy_plot))
    file.write(get_string(energy_analytical_plot))
    file.write(get_string(variance_plot))
    file.write(get_string(var_analytical_plot))
    file.write(get_string(alpha_plot))
    file.close()

def main():
    ##----------------------------------------------------------------------------------------------------------------------
    ''' Main (Body) portion of the code.'''
    ##----------------------------------------------------------------------------------------------------------------------
    ## Paratmeter (interactive)
    ## NOTE: Parameters are such chosen for a faster simulation. Larger parameter may give a better output but will take longer.
    alpha = .05            # Variational Parameter
    alpha_iterations = 250  # Number of iterations of updating the values of alpha
    MC_Sweep = 5000         # Number of Monte Carlo sweeps for calculation
    random_walk = 50       # Number of Monte Carlo Random Walk iterations for calculation
    gamma = .01            # Constant that relates alpha_new with alpha_old
    # Creating an empty array to store value later for plotting
    energy_plot = np.array([])
    alpha_plot = np.array([])
    variance_plot = np.array([])
    energy_analytical_plot = np.array([])
    var_analytical_plot = np.array([])

    ## Generates Iterations for alpha
    for i in range(alpha_iterations):
        e = 0                                   # Upper bound limit of ground state
        e2 = 0                                  # Squared Upper bound limit of ground state
        de_dalpha = 0                           # Derivative of Energy over the population of Random Walks.
        eln = 0                                 # Eln = E_L * (dln(psi_trial)/dalpha)
        ln = 0                                  # ln = (dln(psi_trial)/dalpha)
        error_tolerance = 0  # Maximum admissiable errors or MC step rejections
        for j in range(random_walk):
            e_met, e2_met, eln_met, ln_met, rejections_met = variational_monte_carlo (MC_Sweep, alpha)
            e += e_met / random_walk
            e2 += e2_met / random_walk
            eln += eln_met / random_walk
            ln += ln_met / random_walk
            error_tolerance += rejections_met / random_walk

        ## Analytical expressions for E and variance to compare
        energy_analytical = alpha/2 + 1/8/alpha
        var_analytical = (1 - 4*alpha ** 2) ** 2/32/alpha ** 2

        ## Defines next alpha value
        de_dalpha = 2 * (eln - e * ln)
        ## alpha_new = alpha_old - gamma*dE_dalpha
        alpha = alpha + gamma
        ## Printing values of Alpha, <E>, E_analytical, VarE, Var_analytical for different times on each iterations.
        print('α: %0.4f' % alpha, '||  <E>: %0.4f' % e, '||  E_analytical: % 0.4f' % energy_analytical,
              '||  VarE: %0.4f' % (e2 - e ** 2), '||  Var_analytical: %0.4f' % var_analytical)

        ## Plot variables
        energy_plot = np.append(energy_plot, e)
        alpha_plot = np.append(alpha_plot, alpha)
        variance_plot = np.append(variance_plot, e2 - e ** 2)
        energy_analytical_plot = np.append(energy_analytical_plot, energy_analytical)
        var_analytical_plot = np.append(var_analytical_plot, var_analytical)

    """
    Now save data in a file
    """
    save_in_file(energy_plot,energy_analytical_plot,variance_plot,var_analytical_plot,alpha_plot,)


def read_from_file(name = "data"):
    energy_plot = np.array([])
    alpha_plot = np.array([])
    variance_plot = np.array([])
    energy_analytical_plot = np.array([])
    var_analytical_plot = np.array([])
    with open(name+".txt") as f:
        data = f.readlines()
        for i in data[0].split(",")[:-1]:
            energy_plot = np.append(energy_plot, float(i))
        for i in data[1].split(",")[:-1]:
            energy_analytical_plot = np.append(energy_analytical_plot, float(i))
        for i in data[2].split(",")[:-1]:
            variance_plot = np.append(variance_plot, float(i))
        for i in data[3].split(",")[:-1]:
            var_analytical_plot = np.append(var_analytical_plot, float(i))
        for i in data[4].split(",")[:-1]:
            alpha_plot = np.append(alpha_plot, float(i))

    plot_energy(energy_plot, energy_analytical_plot)
    plot_variance(variance_plot, var_analytical_plot)
    plot_alpha(alpha_plot, energy_plot, energy_analytical_plot)


# calculate and save
main()
# read from file and plot
# read_from_file("good_data_500_50")
read_from_file()


if __name__ == "__main__":
    # main()
    # read_from_file()
    pass