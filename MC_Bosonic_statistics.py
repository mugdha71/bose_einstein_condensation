import random, math, pylab, mpl_toolkits.mplot3d


# 3 dimensional Levy algorithm, used for resampling the positions of entire permutation cycles of bosons
# to sample positions
import numpy
import os


def levy_harmonic_path(k, beta):
    # direct sample (rejection-free) three coordinate values, use diagonal density matrix
    # k corresponds to the length of the permutation cycle
    xk = tuple([random.gauss(0.0, 1.0 / math.sqrt(2.0 *
                                                  math.tanh(k * beta / 2.0))) for d in range(3)])
    x = [xk]  # save the 3 coordinate values xk into a 3d vector x (final point)
    for j in range(1, k):  # loop runs through the permutation cycle
        # Levy sampling (sample a point given the latest sample and the final point)
        Upsilon_1 = (1.0 / math.tanh(beta) +
                     1.0 / math.tanh((k - j) * beta))
        Upsilon_2 = [x[j - 1][d] / math.sinh(beta) + xk[d] /
                     math.sinh((k - j) * beta) for d in range(3)]
        x_mean = [Upsilon_2[d] / Upsilon_1 for d in range(3)]
        sigma = 1.0 / math.sqrt(Upsilon_1)
        dummy = [random.gauss(x_mean[d], sigma) for d in range(3)]  # direct sample the j'th point
        x.append(tuple(dummy))  # construct the 3d path (permutation cycle) by appending tuples
    return x


# (Non-diagonal) harmonic oscillator density matrix, used for organising the exchange of two elements
# to sample permutations
def rho_harm(x, xp, beta):
    Upsilon_1 = sum((x[d] + xp[d]) ** 2 / 4.0 *
                    math.tanh(beta / 2.0) for d in range(3))
    Upsilon_2 = sum((x[d] - xp[d]) ** 2 / 4.0 /
                    math.tanh(beta / 2.0) for d in range(3))
    return math.exp(- Upsilon_1 - Upsilon_2)

def start(T = 0.3):
    N = 512  # number of bosons
    T_star = T
    beta = 1.0 / (T_star * N ** (1.0 / 3.0))  # ??
    nsteps = 1000
    try:
        os.mkdir("imgs_nsteps_%i" % nsteps)
    except:
        pass
    positions = {}  # initial position dictionary
    for j in range(N):  # loop over all particles, initial permutation is identity (k=1)
        a = levy_harmonic_path(1, beta)  # initial positions (outputs a single 3d point)
        positions[a[0]] = a[0]  # positions of particles are keys for themselves in the initial position dict.
    for step in range(nsteps):
        index_pos = list(positions.keys())[random.randint(0,len(positions)-1)]
        boson_a = positions[index_pos]#random.choice(positions.keys())  # randomly pick the position of boson "a" from the dict.
        perm_cycle = []  # initialise the permutation cycle
        while True:  # compute the permutation cycle of the boson "a":
            perm_cycle.append(boson_a)  # construct the permutation cycle by appending the updated position of boson "a"
            boson_b = positions.pop(boson_a)  # remove and return (pop) the position of "a", save it as a temp. var.
            if boson_b == perm_cycle[0]:
                break  # if the cycle is completed, break the while loop
            else:
                boson_a = boson_b  # move boson "a" to position of "b" and continue permuting
        k = len(perm_cycle)  # length of the permutation cycle
        # SAMPLE POSITIONS:
        perm_cycle = levy_harmonic_path(k, beta)  # resample the particle positions in the current permutation cycle
        positions[perm_cycle[-1]] = perm_cycle[
            0]  # assures that the new path is a "cycle" (last term maps to the first term)
        for j in range(len(perm_cycle) - 1):  # update the positions of bosons
            positions[perm_cycle[j]] = perm_cycle[j + 1]  # construct the "cycle": j -> j+1
        # SAMPLE PERMUTATION CYCLES by exchanges:
        # Pick two particles and attempt an exchange to sample permutations (with Metropolis acceptance rate):
        a_1 = random.choice(list(positions.keys()))  # pick the first random particle
        b_1 = positions.pop(a_1)  # save the random particle to a temporary variable
        a_2 = random.choice(list(positions.keys()))  # pick the second random particle
        b_2 = positions.pop(a_2)  # save the random particle to a temporary variable
        weight_new = rho_harm(a_1, b_2, beta) * rho_harm(a_2, b_1, beta)  # the new Metropolis acceptance rate
        weight_old = rho_harm(a_1, b_1, beta) * rho_harm(a_2, b_2, beta)  # the old Metropolis acceptance rate
        if random.uniform(0.0, 1.0) < weight_new / weight_old:
            positions[a_1] = b_2  # accept
            positions[a_2] = b_1
        else:
            positions[a_1] = b_1  # reject
            positions[a_2] = b_2

    # Figure output:
    fig = pylab.figure()
    ax = mpl_toolkits.mplot3d.axes3d.Axes3D(fig)
    ax.set_aspect('equal')
    list_colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    n_colors = len(list_colors)
    dict_colors = {}
    i_color = 0
    # find and plot permutation cycles:
    while positions:
        x, y, z = [], [], []
        starting_boson = list(positions.keys())[0]
        boson_old = starting_boson
        while True:
            x.append(boson_old[0])
            y.append(boson_old[1])
            z.append(boson_old[2])
            boson_new = positions.pop(boson_old)
            if boson_new == starting_boson:
                break
            else:
                boson_old = boson_new
        len_cycle = len(x)
        if len_cycle > 2:
            x.append(x[0])
            y.append(y[0])
            z.append(z[0])
        if len_cycle in dict_colors:
            color = dict_colors[len_cycle]
            ax.plot(x, y, z, color + '+-', lw=0.75)
        else:
            color = list_colors[i_color]
            i_color = (i_color + 1) % n_colors
            dict_colors[len_cycle] = color
            ax.plot(x, y, z, color + '+-', label='k=%i' % len_cycle, lw=0.75)
    # finalize plot
    pylab.title('$N=%i$, $T^*=%04.2f$' % (N, T_star))
    pylab.legend()
    ax.set_xlabel('$x$', fontsize=16)
    ax.set_ylabel('$y$', fontsize=16)
    ax.set_zlabel('$z$', fontsize=16)
    ax.set_xlim3d([-6,6])
    ax.set_ylim3d([-6,6])
    ax.set_zlim3d([-6,6])
    print('snapshot_bosons_3d_N%04i_Tstar%04.2f.png' % (N, T_star))
    # pylab.savefig('imgs_nsteps_%i/snapshot_bosons_3d_N%04i_Tstar%04.2f.svg' % (nsteps,N, T_star))
    pylab.pause(1)
    pylab.close()
    # pylab.show()



start(.1)
start(.1)
start(.1)
start(.1)
start(.1)

# for i in numpy.arange(0.3,3.3,0.1):
#     print(i)
#     start(i)
if __name__ == '__main__':
    pass