import math, numpy as np,matplotlib.pyplot as plt

def z(k, beta):
    return 1.0 / (1.0 - math.exp(- k * beta)) ** 3 #partition function of a single particle in a harmonic trap

def z_prime(k,beta):
    return -3*z(k,beta)*(k*math.exp(-k*beta))/(1.0 - math.exp(- k * beta))

def canonic_recursion(N, beta): #Landsberg recursion relations for the partition function of N bosons
    Z = [1.0] #Z_0 = 1
    for M in range(1, N + 1):
        Z.append(sum(Z[k] * z(M - k, beta) \
                     for k in range(M)) / M)
    return Z #list of partition functions for boson numbers up to N


def canonic_recursion_prime(Z, N, beta): #Landsberg recursion relations for the partition function of N bosons
    Z_prime = [0.0] #Z_0' = 0
    for M in range(1, N + 1):
        Z_prime.append(sum((Z_prime[k] * z(M - k, beta) + z_prime(M-k,beta)*Z[k]) \
                     for k in range(M)) / M)
    return Z_prime #list of partition functions for boson numbers up to N

def N_vs_T(N,T):
    # N = 256 #number of bosons
    # T = 0.1 #temperature
    beta = 1.0 / N ** (1.0 / 3.0) / T
    Z = canonic_recursion(N, beta) #partition function
    Z_prime = canonic_recursion_prime(Z,N,beta)
    N0 = sum(Z[:-1])/Z[-1]
    E_mean = -sum(Z_prime[:-1])/(Z[-1]*N)
    return N0/N, E_mean
location = "imgs/" + str("600") + "_" + str(1000000)
if __name__ == '__main__':
    Ns = [100,200,400]
    nn = []
    tn = []
    legend = []
    E_means = []
    for n in Ns:
        n0 = []
        t_n = []
        E = []
        legend.append("N = "+str(n))
        Ts = np.linspace(0.1,1, 100)
        for t in Ts:
            N0_N,E_mean = N_vs_T(n,t)
            n0.append(N0_N)
            E.append(E_mean)
            t_n.append(t)#/n**(1/3.0))
        nn.append(n0)
        tn.append(t_n)
        E_means.append(E)
    for n, t in zip(nn, tn):
        plt.plot(t, n)
        plt.xlabel("T")
        plt.ylabel("N\u2080/N")
        plt.legend(legend)
    plt.show()


    plt.savefig(location+'/N0_N_vs_T.svg')
    plt.close()

    for e, t in zip(E_means, tn):
        plt.plot(t, e)
        plt.xlabel("T")
        plt.ylabel("\u27E8E\u27E9")
        plt.legend(legend)

    plt.show()
    plt.savefig(location + '/E_mean_vs_T.svg')
