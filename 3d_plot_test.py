import numpy as np
import pylab
from mpl_toolkits.mplot3d import Axes3D
from scipy import stats
import matplotlib.pyplot as plt
test_x = []
test_y = []

with open('x.txt', 'r') as f:
    for line in f.readlines():
        test_x.append(float(line))
with open('y.txt', 'r') as f:
    for line in f.readlines():
        test_y.append(float(line))

xy = np.vstack([test_x, test_y])
density = stats.gaussian_kde(xy)
nbins = 500

xi, yi = np.mgrid[min(test_x):max(test_x):nbins * 1j, min(test_y):max(test_y):nbins * 1j]
di = density(np.vstack([xi.flatten(), yi.flatten()]))
fig = plt.figure()
ax = Axes3D(fig)
ax.plot_surface(xi, yi, di.reshape(xi.shape), cmap=pylab.cm.viridis,
                linewidth=0, antialiased=False)
# plt.xlabel('$x$')
# plt.ylabel('$y$')
# plt.title('The distribution of the $x$ and $y$ positions')
# plt.colorbar()
# plt.xlim(-3.0, 3.0)
# plt.ylim(-3.0, 3.0)
plt.show()

if __name__ == '__main__':
    pass