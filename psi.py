import math
import matplotlib.pyplot as plt


def psi(x):
    y = [math.exp(-x ** 2 / 2.0) / math.pi ** 0.25]  # ground state
    return y  # first excited state

x = [i * 0.1 for i in range(-30, 31)]
y = [psi(i) for i in x]
yy = [i[0]*i[0] for i in y]


# plt.plot(x,y,'--b')
plt.plot(x,yy)
plt.xlabel("Position x")
plt.ylabel("|\u03A8(x)|\u00B2")
# plt.legend(["|\u03A8|\u00B2"])
location = "imgs/" + str("600") + "_" + str(1000000)
plt.savefig(location + '/psi_pow(2)h.svg')
plt.show()

if __name__ == '__main__':
    pass